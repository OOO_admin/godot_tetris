extends Control

@onready var scoreLabel:= $Score
@onready var level := $Levels
@onready var speedLabel := $Speed

var lines:=0
var score:=0
var speed:=1

#消行之后增加
func add_level(value:int):
	lines+= value
	level.text = str(lines)
	score+=Globals.add_score[value-1]
	scoreLabel.text = str(score)

func reset():
	lines = 0
	score = 0
	level.text = str(lines)
	scoreLabel.text = str(score)	

func check_speed():
	var index := Globals.level_score.size()-1
	var temp_speed := 1
	for i in Globals.level_score.size():
		if score < Globals.level_score[index]:
			index -= 1
		else:
			temp_speed = index + 1
	
	if speed != temp_speed:
		speed = temp_speed
		speedLabel.text = str(speed)
		owner.set_wait_time(1-index*0.1)

