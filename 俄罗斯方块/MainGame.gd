extends Node2D

const START_POS :Vector2 = Vector2(3,0)
const DISTANCE := Vector2(0,1)

@onready var nextTileMap:TileMap = $NextTileMap
@onready var mainTileMap:TileMap = $MainTileMap
@onready var tick:Timer = $Tick
@onready var lateral_tick:Timer = $Lateral_Tick
@onready var gui:= $GUI

var nextShapeIndex:int = 1
var nextShape:Array
#当前形状的index
var currentShapeIndex : int = 1
var currentShape : Array
#已经落地的型状
var landingTiles:Array = []
#平移
var lateral_move := 0
#状态
var status_index := 0
#最终会显示的样子
var final_location_shape : Array = []
var isFastDown := false 

func _ready() -> void:
	randomize()
	get_next_shape()
	draw_next_shape()
	delete_next_shape()
	get_current_shape()
	draw_current_shape()
	final_location()
	get_next_shape()
	draw_next_shape()
	tick.start()
	lateral_tick.start()
	gui.reset()

func _process(delta: float) -> void:
	lateral_move = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if Input.is_action_pressed("ui_down"):
		tick.wait_time = 0.1
		isFastDown = true
	if Input.is_action_just_released("ui_down"):
		tick.wait_time = 1
		isFastDown = false
	if Input.is_action_just_pressed("rotate"):
		get_rotate()
		final_location()
	if Input.is_action_just_pressed("fast_down"):
		get_fast_down()

func get_fast_down():
	var depth = 1
	var currentCopy = []
	for cell in currentShape:
		cell.y += depth
		currentCopy.append(cell)
	
	var is_land = check_landing(currentCopy)
	while not is_land:
		depth += 1
		currentCopy = []
		for cell in currentShape:
			cell.y += depth
			currentCopy.append(cell)
		is_land = check_landing(currentCopy)
		
	for i in currentCopy.size():
		currentCopy[i].y -= 1
		mainTileMap.set_cell(0,currentCopy[i],0,Vector2.ZERO)
	landingTiles.append_array(currentCopy)

	delete_current_shape()
	get_current_shape()
	draw_current_shape()
	delete_next_shape()
	get_next_shape()
	draw_next_shape()
	check_full_lines()
	final_location()
	draw_landing_tiles()
	check_game_over()
	$audio/shift.play()

func get_next_shape():
	nextShapeIndex = randi() % 7 + 1
	status_index = 0
	nextShape = Globals.shapes["shape"+str(nextShapeIndex)]
	
func draw_next_shape():
	for cell in nextShape:
		nextTileMap.set_cell(0,cell,0,Vector2.ZERO)
		
func delete_next_shape():
	for cell in nextShape:
		nextTileMap.set_cell(0,cell,-1)

func get_current_shape():
	status_index = 0 
	currentShapeIndex = nextShapeIndex
#	currentShape = nextShape
	currentShape = []
	for cell in nextShape:
		currentShape.append(cell+START_POS)

func draw_current_shape():
	for cell in currentShape:
		mainTileMap.set_cell(0,cell,0,Vector2.ZERO)

func delete_current_shape():
	for cell in currentShape:
		mainTileMap.set_cell(0,cell,-1)

func move_current_shape():
	delete_current_shape()
	var currentCopy:Array = []
	for cell in currentShape:
		cell += DISTANCE
		currentCopy.append(cell)
		
	var is_land := check_landing(currentCopy)
	if is_land:
		landingTiles.append_array(currentShape)
		
		delete_current_shape()
		get_current_shape()
		delete_next_shape()
		get_next_shape()
		draw_next_shape()
		check_full_lines()
		final_location()
		draw_current_shape()
		draw_landing_tiles()
		check_game_over()
		$audio/land.play()
	else:
		currentShape = currentCopy 
	if isFastDown:
		$audio/move.play()
	
func draw_landing_tiles():
	for cell in landingTiles:
		mainTileMap.set_cell(0,cell,0,Vector2.ZERO)

func check_landing(copy:Array) -> bool:
	#落在地面
	var is_land:bool = false
	var max_y := get_max_y(copy)
	if max_y > 19:
		is_land = true
	
	#落在其他形状上
	var overlapp := check_overlapping(copy)
	
	return is_land or overlapp
	
#检查重叠
func check_overlapping(copy:Array) -> bool:
	var overlapp := false
	for cell in copy:
#		if landingTiles.find(cell) > -1:
		if cell in landingTiles:
			overlapp = true
			break
	return overlapp

func check_bound(copy:Array) -> bool:
	return (get_min_x(copy) < 0 or get_max_x(copy) > 9)  

#判断是否已经满足消行
func check_full_lines():
	#一行有多少个块
	var line_elements : int = 0
	#第几行
	var line:int = 19  
	#当前行所有数据的位置
	var tileIndex:Array = []
	var fullLineCount = 0
	while line > - 1:
		line_elements = 0 
		tileIndex = []
		for i in landingTiles.size():
			var cell : Vector2 = landingTiles[i]
			if cell.y == line:
				line_elements += 1
				tileIndex.append(i)
		if line_elements == 10 :
			fullLineCount += 1
			delete_landing_data(tileIndex,line)
		else:
			line -= 1
	if fullLineCount > 0 :
		delete_landing_tile()
		draw_landing_tiles()
		draw_current_shape()
		$audio/clear.play()
		gui.add_level(fullLineCount)
		
		
func check_game_over():
	for cell in landingTiles:
		if cell.y == 0:
			tick.stop()
			lateral_tick.stop()
			currentShape = []
			$audio/gameover.play()

func delete_landing_data(tileIndex:Array,line:int):
#	var last :int = tileIndex.size() - 1
	while tileIndex.size() > 0:
		landingTiles.remove_at(tileIndex.pop_back())
	for i in landingTiles.size():
		if landingTiles[i].y < line:
			landingTiles[i].y += 1

func delete_landing_tile():
	var cells:Array = mainTileMap.get_used_cells_by_id(0)
	for cell in cells:
		mainTileMap.set_cell(0,cell,-1)
		

func get_min_y(copy:Array) -> int:
	var min := 200
	for cell in copy:
		if cell.y < min:
			min = cell.y 
	return min

func get_max_y(copy:Array) -> int:
	var max:int = 0
	for cell in copy:
		if cell.y > max:
			max = cell.y
	return max 

func get_min_x(copy:Array) -> int:
	var min:= 200
	for cell in copy:
		if cell.x < min:
			min = cell.x
	return min

func get_max_x(copy:Array) -> int:
	var max :=0
	for cell in copy:
		if cell.x > max:
			max = cell.x 
	return max 

#下落
func _on_tick_timeout() -> void:
	move_current_shape()
	draw_current_shape()
	
func lateral_move_current_shape():
	delete_current_shape()
	var currentCopy:Array = []
	for cell in currentShape:
		cell.x += lateral_move
		currentCopy.append(cell)
	if check_bound(currentCopy) or check_overlapping(currentCopy):
		pass 
	else:	
		currentShape = currentCopy
	$audio/move.play()

func get_rotate():
	delete_current_shape()
	rotate_current_shape()
	final_location()
	draw_current_shape()
	$audio/rotate.play()
	
func rotate_current_shape():
	var min_x := get_min_x(currentShape)
	var min_y := get_min_y(currentShape)
	
	var status_list  = Globals.shapes_state[currentShapeIndex-1]
	
	var temp_index := status_index + 1
	if temp_index >= status_list.size():
		temp_index = 0 
	
	var status :Array = status_list[temp_index].duplicate()
	for i in status.size():
		status[i].x += min_x
		status[i].y += min_y
		
	var isBound := check_bound(status)
	var isLanding := check_landing(status)
	
	if not isBound and not isLanding:
		currentShape = status
		status_index = temp_index

func _on_lateral_tick_timeout() -> void:
	if lateral_move != 0:
		lateral_move_current_shape()
		final_location()
		draw_current_shape()
		
#会出现的位置
func final_location():
	delete_final_shape()
	var depth := 1
	var currentCopy := []
	for cell in currentShape:
		cell.y += depth
		currentCopy.append(cell)
	
	var is_land:bool = check_landing(currentCopy)
	
	while not is_land:
		depth += 1
		currentCopy = []
		for cell in currentShape:
			cell.y += depth
			currentCopy.append(cell)
		is_land = check_landing(currentCopy)
	final_location_shape = []
	for cell in currentCopy:
		cell.y -= 1
		final_location_shape.append(cell)
		
	draw_final_shape()

func draw_final_shape():
	for cell in final_location_shape:
		mainTileMap.set_cell(0,cell,1,Vector2.ZERO)

func delete_final_shape():
	for cell in final_location_shape:
		mainTileMap.set_cell(0,cell,-1)
		
func set_wait_time(value):
	tick.stop()
	tick.wait_time = value
	tick.start()
	$audio/level.play()
