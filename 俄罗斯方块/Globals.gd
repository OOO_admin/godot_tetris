extends Node

var shapes := {
	"shape1":[Vector2(0,0),Vector2(0,1),Vector2(1,1),Vector2(2,1)],
	"shape2":[Vector2(2,0),Vector2(0,1),Vector2(1,1),Vector2(2,1)],
	"shape3":[Vector2(0,0),Vector2(1,0),Vector2(1,1),Vector2(2,1)],
	"shape4":[Vector2(1,0),Vector2(2,0),Vector2(0,1),Vector2(1,1)],
	"shape5":[Vector2(1,0),Vector2(0,1),Vector2(1,1),Vector2(2,1)],
	"shape6":[Vector2(0,0),Vector2(1,0),Vector2(2,0),Vector2(3,0)],
	"shape7":[Vector2(0,0),Vector2(1,0),Vector2(0,1),Vector2(1,1)],	
}



var shape1_state := [
	[Vector2(0,0),Vector2(0,1),Vector2(1,1),Vector2(2,1)],
	[Vector2(1,0),Vector2(1,1),Vector2(0,2),Vector2(1,2)],
	[Vector2(0,0),Vector2(1,0),Vector2(2,0),Vector2(2,1)],
	[Vector2(0,0),Vector2(0,1),Vector2(0,2),Vector2(1,0)],
]

var shape2_state := [
	[Vector2(2,0),Vector2(0,1),Vector2(1,1),Vector2(2,1)],
	[Vector2(0,0),Vector2(1,0),Vector2(1,1),Vector2(1,2)],
	[Vector2(0,0),Vector2(1,0),Vector2(2,0),Vector2(0,1)],
	[Vector2(0,0),Vector2(0,1),Vector2(0,2),Vector2(1,2)]
]

var shape3_state := [
	[Vector2(0,0),Vector2(1,0),Vector2(1,1),Vector2(2,1)],
	[Vector2(1,0),Vector2(0,1),Vector2(1,1),Vector2(0,2)],
]

var shape4_state :=[
	[Vector2(1,0),Vector2(2,0),Vector2(0,1),Vector2(1,1)],
	[Vector2(0,0),Vector2(0,1),Vector2(1,1),Vector2(1,2)],
]

var shape5_state := [
	[Vector2(1,0),Vector2(0,1),Vector2(1,1),Vector2(2,1)],
	[Vector2(1,0),Vector2(0,1),Vector2(1,1),Vector2(1,2)],
	[Vector2(0,1),Vector2(1,1),Vector2(2,1),Vector2(1,2)],
	[Vector2(1,0),Vector2(1,1),Vector2(2,1),Vector2(1,2)]
]

var shape6_state :=[
	[Vector2(0,1),Vector2(1,1),Vector2(2,1),Vector2(3,1)],
	[Vector2(1,0),Vector2(1,1),Vector2(1,2),Vector2(1,3)],
]

var shape7_state :=[
	[Vector2(0,0),Vector2(1,0),Vector2(0,1),Vector2(1,1)]
]

var shapes_state := [
	shape1_state,
	shape2_state,
	shape3_state,
	shape4_state,
	shape5_state,
	shape6_state,
	shape7_state,
]

#消行增加的分数
var add_score:=[100,300,700,1500]

var level_score:= [0,10000,20000,30000,50000,70000,90000,110000,130000,150000]

