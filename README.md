# 俄罗斯方块

最后编写：`2023-10-31` `万圣节快乐`

工具: `Godot 4x`

本笔记用于记录与分享在学习和开发俄罗斯方块中学到的知识

## 使用自动加载加载全局配置
![](./assets/1_8vbeij350.png)
![](./assets/2_k8ej1r891.png)
在`项目设置 - 自动加载`中将需要的脚本路径填写好，就可以在其他脚本中直接通过`名称.属性名`的方式进行访问

## 通过代码设置tileMap
简单来说就是一行代码

```python
var layer = 0                   #在Tilemap的指定层绘制瓦片
var pos = Vector2(3,4)          #瓦片将要绘制的位置
var source = 1                  #瓦片所在的图集编号
var source_pos = Vector2(0,0)   #瓦片在图集中的位置
TileMap.set_cell(layer,pos,source,source_pos)
```
当然我们也可以用下面一行代码来清空
```python
var layer = 0
var pos = Vector(3,4)
var source = -1
TileMap.set_cell(layer,pos,source) 
```
只需要指定绘制的瓦片编号为`-1`即可